package biblioteca.util;

import biblioteca.model.Carte;

public class Validator {
	
	public static boolean isStringOK(String s) throws Exception{
		boolean flag = s.matches("[a-zA-Z]+");
		if(!flag)
			throw new Exception("String invalid");
		return flag;
	}
	
	public static void validateCarte(Carte c)throws Exception{


		if(c.getAnAparitie().length()!=4){
			throw new Exception("Anul aparitiie nu este format din 4 cifre");
		}
		if(c.getTitlu().length()>20){
			throw new Exception("Titlul trebuie sa fie format din maxim 20 de caractere");
		}

		if(c.getCuvinteCheie()==null){
			throw new Exception("Lista cuvinte cheie vida!");
		}

		if(c.getAutori()==null){
			throw new Exception("Lista autori vida!");
		}
		if(!isStringOK(c.getTitlu()))
			throw new Exception("Titlu invalid!");
		for(String s:c.getAutori()){
			if(!isStringOK(s))
				throw new Exception("Autor invalid!");
		}
		for(String s:c.getCuvinteCheie()){
			if(!isStringOK(s))
				throw new Exception("Cuvant cheie invalid!");
		}
		if(!Validator.isNumber(c.getAnAparitie()))
			throw new Exception("Editura invalid!");

	}
	
	public static boolean isNumber(String s){
		return s.matches("[0-9]+");
	}

}
